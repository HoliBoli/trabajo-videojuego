#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <conio.h>

using namespace std;
struct Items{
	string codigo;
	string tipo;
	string nombre_item;
	int unidades_vendidas;
	float precio;
};
void leer_password(string *pass){
	int i=0, n;
	string aux;
	char c;
	if(c!=8){
		c = getch();
	}
    while(c != 13){
    	if(c!=8){
    		aux = aux + c;
    		i++;
    		cout << '*';
		}else if(i>0){
			n=aux.length();
			cout << (char)8 << (char)32 << (char)8;
			aux.erase(n-1,1);	
			i--;
		}
    	c = getch();
	}
	*pass=aux;
}

const int id = 100000;

int ID(){
	int num;
	string linea;
	ifstream archivo;
	archivo.open("jugadores.txt",ios::in);
	
	if(!archivo.fail()){
		while(getline(archivo,linea)){
			num++;
		}
		archivo.close();

	}else{
		cout << "No se pudo abrir el archivo";
	}
	return id+num;
}

struct d_jugador{
	int id;
	string n_jugador;
	string n_usuario;
	string password;
	string pais;
	string equipo;
	int nivel;
	int puntos;
	float saldo;
};

vector <struct d_jugador> listaJugadores;

struct d_jugador leerRegistroJugadores(string linea){
	stringstream tmp(linea);
	string campo;
	struct d_jugador resultado;
	getline(tmp,campo,',');
	resultado.id = atoi(campo.c_str());
	getline(tmp,resultado.n_jugador,',');
	getline(tmp,resultado.n_usuario,',');
	getline(tmp,resultado.password,',');
	getline(tmp,resultado.pais,',');
	getline(tmp,resultado.equipo,',');
	getline(tmp,campo,',');
	resultado.nivel = atoi(campo.c_str());
	getline(tmp,campo,',');
	resultado.puntos = atoi(campo.c_str());
	getline(tmp,campo,',');
	resultado.saldo = atof(campo.c_str());
	return resultado;
} 

string archivoJugadores = "jugadores.txt";

void cargarJugadores(){
	ifstream archivo(archivoJugadores.c_str());
	string linea;
	if(archivo.is_open()){
		struct d_jugador jug;
		while(getline(archivo,linea)){
			jug = leerRegistroJugadores(linea);
			listaJugadores.push_back(jug);
		}
	}else{
		cout << "Error al abrir archivo";
		return;
	}
	archivo.close();
}

string opciones[][5] = {{"\t\t 1) INICIAR SESION", "\t\t 2) CREAR UNA CUENTA","\t\t 3) VER RANKING DE JUGADORES","\t\t 4) VER TIENDA EN LINEA", "\t\t 9) Salir"},
						{"\t\t 1) Editar Datos ", "\t\t 2) Ver estadisticas", "\t\t 3) Ver Inventario", "\t\t 9) Salir"},
						{"\t\t 1) Confirmar  ","\t\t 9) Salir"}};
		
int cantidadOpciones[] = { 5, 4, 2, 3 };

const int OPCION_SALIR = 9;
const int OPCION_MENU_PRINCIPAL = 0;

void mostrarEncabezadoJuego(){
	system("cls");
	cout << endl;
	cout << "\t\t******************************" << endl;
	cout << "\t\t*         DRAGON SLAYER      *" << endl;
	cout << "\t\t******************************" << endl;

}

void mostrarMenu(string *listaCadenas, int cantidadElementos) {
	for (int i = 0; i < cantidadElementos; i++) {
		cout << *(listaCadenas+i)<< endl;
	}
}

void mostrarDatosUsuario(string usuario){
	ifstream archivo(archivoJugadores.c_str());
	string linea;
	struct d_jugador registro;
	if(archivo.is_open()){
		while(!archivo.eof()){
			getline(archivo,linea);
			registro = leerRegistroJugadores(linea);
			if(usuario==registro.n_usuario){
				
				cout << "\t\t ID:" << endl;
				cout << "\t\t   " << registro.id << endl;
				cout << "\t\t Nombre de Usuario: " << endl;
				cout << "\t\t   " << registro.n_usuario << endl;
				cout << "\t\t Pais: " << endl;
				cout << "\t\t   " << registro.pais << endl;
				cout << "\t\t Equipo: " << endl;
				cout << "\t\t   " << registro.equipo << endl;
				cout << "\t\t Nivel: " << endl;
				cout << "\t\t   " << registro.nivel << endl;
				cout << "\t\t Puntos: " << endl;
				cout << "\t\t   " << registro.puntos << endl;
				cout << "\t\t Saldo: " << endl;
				cout << "\t\t   " << registro.saldo << endl;
			}
		}
	}else{
		cout << "Error al abrir archivo";
	}
}

void validaIngreso(string usuario, string *pass, int *cond){
	int r=0;
	ifstream archivo(archivoJugadores.c_str());
	string linea;
	struct d_jugador registro;
	if(archivo.is_open()){
		while(!archivo.eof()){
			getline(archivo,linea);
			registro = leerRegistroJugadores(linea);
			if(usuario==registro.n_usuario and *pass==registro.password){
				*cond = 0;
				mostrarEncabezadoJuego();
				mostrarDatosUsuario(usuario);
				mostrarMenu(opciones[1],cantidadOpciones[1]);
			}
		}
	}else{
		cout<<"Error al abrir archivo";
	}
}

bool validar_contrasenia(string new_password1, string new_password2){
	if(new_password1 == new_password2) return false;
	else return true;
	
}

bool validar_pais(string new_country){
	string pais;
	bool r=true;
	ifstream archivo;
	archivo.open("paises_del_mundo.txt", ios::in);
	while(!archivo.eof()){
		getline(archivo,pais);
		if(pais == new_country){
			r =false;
			break;
		}
	} 
	archivo.close();
	return r;
}

bool validar_usuario(string nombre_archivo, string new_user){
	string linea, name, user, id;
	bool r = false;
	int i=0;
	ifstream archivo("jugadores.txt",  ios::in);
	
	while(!archivo.eof()){
		getline(archivo,linea);
		stringstream tmp(linea);
		getline(tmp,id,',');
		getline(tmp,name,',');
		getline(tmp,user,',');
		if(user == new_user){
			r=true;
			break;
		}
		i++;
	}
	archivo.close();
	return r;
}

void opcionIniciarSesion(int *cond, string *pass){
	string usuario;
	cout << "\t\t Nombre de usuario: ";
	cin.ignore();
	getline(cin,usuario);
	cout << "\n\t\t Contrasenia: ";
	leer_password(pass);
	cout << endl;
	validaIngreso(usuario,pass,cond);
}

void modificaciones(){
	ofstream aux;
    ifstream lectura;
    int encontrado=1;
    string linea, xid, xnombre, xusuario, nusuario, npais, nequipo, confirm,id,n_jugador,n_usuario,password,pais,equipo,nivel,puntos,saldo;
    aux.open("auxiliar.txt",ios::app);
    lectura.open("jugadores.txt",ios::in);
    if(aux.is_open() && lectura.is_open()){
    	mostrarEncabezadoJuego();
    	cout<<endl;
        cout<<"\t\t Ingresa ID: ";
        cin.ignore(); getline(cin,xid);
        cout<<"\t\t Ingrese nombre: ";
        cin.ignore(); getline(cin,xnombre);
        cout<<"\t\t Ingrese nombre de usuario: ";
        cin.ignore(); getline(cin,xusuario);
        while(!lectura.eof()){
        	getline (lectura,linea);
        	stringstream tmp(linea);
		    getline(tmp,id,',');
			getline(tmp,n_jugador,',');
			getline(tmp,n_usuario,',');
			getline(tmp,password,',');
			getline(tmp,pais,',');
			getline(tmp,equipo,',');
			getline(tmp,nivel,',');
			getline(tmp,puntos,',');
			getline(tmp,saldo,'\n');  	
			if(id == xid){
				if(n_jugador == xnombre){
					if(n_usuario == xusuario){
						encontrado=0;
						if(encontrado==0){
							cout<<"\t\t Usuario encontrado"<<endl;
						}
		                cout<<"\t\t Desea cambiar nombre de usuario? <S/N> "<<endl;
		                cin>>confirm;
		                if(confirm[0]=='s'||confirm[0]=='S'){
		                	
		                	cout<<"\t\t Ingrese nuevo nombre de usuario"<<endl;
		                	cin>>nusuario;
		                	cout<<"\t\t Confirmar operacion <S/N> "<<endl;
		                	cin>>confirm;
		                	if(confirm[0]=='s'||confirm[0]=='S'){
		                		n_usuario=nusuario;
		                	}
						}
		              	  cout<<"\t\t Desea cambiar el pais? <S/N> " <<endl;
		              	  cin>>confirm;
		                if(confirm[0]=='s'||confirm[0]=='S'){
		                	cout<<"\t\t Ingrese nuevo pais"<<endl;
		                	cin>>npais;
		                	cout<<"\t\t Confirmar operacion <S/N> "<<endl;
		                	cin>>confirm;
		                	if(confirm[0]=='s'||confirm[0]=='S'){
		                		pais=npais;
		                	}
						}
		                cout<<"\t\t Desea cambiar de equipo? <S/N> "<<endl;
		                cin>>confirm;
		                if(confirm[0]=='s'||confirm[0]=='S'){
		                	cout<<"\t\t Ingrese nuevo nombre equipo"<<endl;
		                	cin>>nequipo;
		                	cout<<"\t\t Confirmar operacion <S/N> "<<endl;
		                	cin>>confirm;
		                	if(confirm[0]=='s'||confirm[0]=='S'){
		                		equipo=nequipo;
		                	}
						}
		                
            		}
				}
			}
            aux<<id<<","<<n_jugador<<","<<n_usuario<<","<<password<<","<<pais<<","<<equipo<<","<<nivel<<","<<puntos<<","<<saldo<<endl;
        }
        if(encontrado==1){
        cout<<"\t\t No se encontro ningun usuario con esos datos"<<endl;
    	}
    }else{
        cout<<"\t\t No se pudo abrir el Archivo o aun no ha sido creado"<<endl;
    }
    
    aux.close();
    lectura.close();
}
//toda esta parte de aca es mia (akira) desde ACA:         falta resumirlo, o abreviarlo y optimizrlo, pa k sean menos lineas(estetica)
struct da_jugador{
	string id;
	string n_jugador;
	string n_usuario;
	string password;
	string pais;
	string equipo;
	string nivel;
	string puntos;
	string saldo;
};

void buscador_x_id(){
	int asd;
	asd=0;
	struct da_jugador gamer;
	string ID, linea,confirm;	
	while(asd==0){	
		system("cls");
		mostrarEncabezadoJuego();
		ifstream lectura;
		lectura.open("jugadores.txt",ios::in);
		cout<<endl;
		cout<<"\t\t Ingrese el ID en el buscador";
		cin>>ID;
		while(!lectura.eof()){
			getline (lectura,linea);
		    stringstream tmp(linea);
			getline(tmp,gamer.id,',');
			getline(tmp,gamer.n_jugador,',');
			getline(tmp,gamer.n_usuario,',');
			getline(tmp,gamer.password,',');
			getline(tmp,gamer.pais,',');
			getline(tmp,gamer.equipo,',');
			getline(tmp,gamer.nivel,',');
			getline(tmp,gamer.puntos,',');
			getline(tmp,gamer.saldo,'\n'); 
			if(ID==gamer.id){
				cout<<"\t\t ID encontrada"<<endl;
				cout<<"\t\t ID: "<<gamer.id<<endl;
				cout<<"\t\t Nombre de jugador: "<<gamer.n_jugador<<endl;
				cout<<"\t\t Nombre de usuario: "<<gamer.n_usuario<<endl;
				cout<<"\t\t Password: "<<gamer.password<<endl;
				cout<<"\t\t Pais: "<<gamer.pais<<endl;
				cout<<"\t\t Equipo: "<<gamer.equipo<<endl;
				cout<<"\t\t Nivel: "<<gamer.nivel<<endl;
				cout<<"\t\t Puntos acumulados: "<<gamer.puntos<<endl;
				cout<<"\t\t Saldo: "<<gamer.saldo<<endl;
				asd++;
			}
		}
		if(asd==0){
			cout<<"\t\t No se encontro ID"<<endl;
		}
		cout<<"\t\t Desea buscar otra ID?  <S/N>"<<endl;
		cin>>confirm;
		if(confirm[0]=='s'||confirm[0]=='S'){
			asd=0;
		}else{
			asd=1;
		}
	}
	system("cls");
}

void buscador_x_n_jugador(){
	int asd;
	asd=0;
	struct da_jugador gamer;
	string n_jugador, linea,confirm;
	while (asd==0){
		system("cls");
		mostrarEncabezadoJuego();
		ifstream lectura;
		lectura.open("jugadores.txt",ios::in);
		cout<<endl;
		cout<<"\t\t Ingrese nombre de usuario en el buscador"<<endl;
		//getline(cin,n_jugador);
		cin>>n_jugador;
		asd=0;
		while(!lectura.eof()){
			getline(lectura,linea);
		    stringstream tmp(linea);
			getline(tmp,gamer.id,',');
			getline(tmp,gamer.n_jugador,',');
			getline(tmp,gamer.n_usuario,',');
			getline(tmp,gamer.password,',');
			getline(tmp,gamer.pais,',');
			getline(tmp,gamer.equipo,',');
			getline(tmp,gamer.nivel,',');
			getline(tmp,gamer.puntos,',');
			getline(tmp,gamer.saldo,'\n'); 
			if(n_jugador==gamer.n_jugador){
				cout<<gamer.n_usuario<<"  "<<gamer.pais<<"  "<<gamer.equipo<<"  "<<gamer.nivel<<endl;
				asd=asd+1;
			}
		}	
		if(asd==0){
			cout<<"\t\t No se encontró jugador"<<endl;  
			cout<<"\t\t >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
			asd=1;
		}
		cout<<"\t\t Desea buscar otro jugador?  <S/N>"<<endl;
		cin>>confirm;
		if(confirm[0]=='s'||confirm[0]=='S'){
			asd=0;
		}else{
			asd=1;
		}
	}
	system("cls");
}

void buscador_x_n_usuario(){
	int asd;
	asd=0;
	struct da_jugador gamer;
	string usuario, linea,confirm;
	while (asd==0){
		system("cls");
		mostrarEncabezadoJuego();
		ifstream lectura;
		lectura.open("jugadores.txt",ios::in);
		//getline(cin,usuario);
		cout<<endl;
		cout<<"\t\t Ingrese nombre de usuario en el buscador"<<endl;
		cout<<"\t\t "; cin>>usuario;
		asd=0;
		while(!lectura.eof()){
			getline(lectura,linea);
		    stringstream tmp(linea);
			getline(tmp,gamer.id,',');
			getline(tmp,gamer.n_jugador,',');
			getline(tmp,gamer.n_usuario,',');
			getline(tmp,gamer.password,',');
			getline(tmp,gamer.pais,',');
			getline(tmp,gamer.equipo,',');
			getline(tmp,gamer.nivel,',');
			getline(tmp,gamer.puntos,',');
			getline(tmp,gamer.saldo,'\n'); 
			if(usuario==gamer.n_usuario){
				cout<<gamer.n_usuario<<"  "<<gamer.pais<<"  "<<gamer.equipo<<"  "<<gamer.nivel<<endl;
				asd=asd+1;
			}
		}	
		if(asd==0){
			cout<<"\t\t No se encontró usuario"<<endl;  
			cout<<"\t\t >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
			asd=1;
		}
		cout<<"\t\t Desea buscar otro usuario?  <S/N>"<<endl;
		cin>>confirm;
		if(confirm[0]=='s'||confirm[0]=='S'){
			asd=0;
		}else{
			asd=1;
		}
	}
	system("cls");
}

void buscador_x_pais(){
	struct da_jugador gamer;
	string pais, linea,confirm;
	int asd=0;
	while(asd==0){
		system("cls");
		mostrarEncabezadoJuego();
		ifstream lectura;
		lectura.open("jugadores.txt",ios::in);
		cout<<endl;
		cout<<"\t\t Ingrese el pais en el buscador"<<endl;
		cout<<"\t\t "; cin>>pais;
		while(!lectura.eof()){
			getline (lectura,linea);
		    stringstream tmp(linea);
			getline(tmp,gamer.id,',');
			getline(tmp,gamer.n_jugador,',');
			getline(tmp,gamer.n_usuario,',');
			getline(tmp,gamer.password,',');
			getline(tmp,gamer.pais,',');
			getline(tmp,gamer.equipo,',');
			getline(tmp,gamer.nivel,',');
			getline(tmp,gamer.puntos,',');
			getline(tmp,gamer.saldo,'\n'); 
			if(pais==gamer.pais){
				cout<<gamer.n_usuario<<"  "<<gamer.pais<<"  "<<gamer.equipo<<"  "<<gamer.nivel<<endl;
				asd++;
			}
		}
		if(asd==0){
			cout<<"\t\t No se encontró pais"<<endl;  
			cout<<"\t\t >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
			asd=1;
		}		
		cout<<"\t\t Desea buscar otro pais?  <S/N>"<<endl;
		cin>>confirm;
		if(confirm[0]=='s'||confirm[0]=='S'){

			asd=0;
		}else{
			asd=1;
		}
	}
	system("cls");
}

void buscador_x_equipo(){
	struct da_jugador gamer;
	string equipo, linea,confirm;
	int asd;
	asd=0;
	while(asd==0){
		system("cls");
		mostrarEncabezadoJuego();
		ifstream lectura;
		lectura.open("jugadores.txt",ios::in);
		cout<<endl;
		cout<<"\t\t Ingrese nombre del equipo en el buscador"<<endl;
		cout<<"\t\t "; getline(cin,equipo);
		while(!lectura.eof()){
			getline (lectura,linea);
		    stringstream tmp(linea);
			getline(tmp,gamer.id,',');
			getline(tmp,gamer.n_jugador,',');
			getline(tmp,gamer.n_usuario,',');
			getline(tmp,gamer.password,',');
			getline(tmp,gamer.pais,',');
			getline(tmp,gamer.equipo,',');
			getline(tmp,gamer.nivel,',');
			getline(tmp,gamer.puntos,',');
			getline(tmp,gamer.saldo,'\n'); 
			if(equipo==gamer.equipo){
				cout<<gamer.n_usuario<<"  "<<gamer.pais<<"  "<<gamer.equipo<<"  "<<gamer.nivel<<endl;
				asd++;
			}
		}
		if(asd==0){
			cout<<"\t\t No se encontro equipo"<<endl;  
			cout<<"\t\t >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
			asd=1;
		}		
		cout<<"\t\t ¿Desea buscar otro equipo?  <S/N>"<<endl;
		cin>>confirm;
		if(confirm[0]=='s'||confirm[0]=='S'){

			asd=0;
		}else{
			asd=1;
		}
	}
	system("cls");
}

void buscador_x_nivel(){
	struct da_jugador gamer;
	string nivel, linea,confirm;
	int asd;
	while(asd==0){
		system("cls");
		mostrarEncabezadoJuego();
		ifstream lectura;
		lectura.open("jugadores.txt",ios::in);
		cout<<endl;
		cout<<"\t\t Ingrese el nivel en el buscador"<<endl;
		cout<<"\t\t "; cin>>nivel;
		while(!lectura.eof()){
			getline (lectura,linea);
		    stringstream tmp(linea);
			getline(tmp,gamer.id,',');
			getline(tmp,gamer.n_jugador,',');
			getline(tmp,gamer.n_usuario,',');
			getline(tmp,gamer.password,',');
			getline(tmp,gamer.pais,',');
			getline(tmp,gamer.equipo,',');
			getline(tmp,gamer.nivel,',');
			getline(tmp,gamer.puntos,',');
			getline(tmp,gamer.saldo,'\n'); 
			if(nivel==gamer.nivel){
				cout<<gamer.n_usuario<<"  "<<gamer.pais<<"  "<<gamer.equipo<<"  "<<gamer.nivel<<endl;
				asd++;
			}
		}
		if(asd==0){
			cout<<"\t\t No se encontro equipo"<<endl;  
			cout<<"\t\t >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
			asd=1;
		}		
		cout<<"\t\t Desea buscar otro equipo?  <S/N>"<<endl;
		cin>>confirm;
		if(confirm[0]=='s'||confirm[0]=='S'){

			asd=0;
		}else{
			asd=1;
		}
		system("cls");
	}
}

void buscador_x(){
	
	int condicion, condicion2,continua,continua2;
	condicion=0;
	condicion2=0;
	continua=0;
	continua2=0;
	int clave;
	string clave2,contra,contra_administrador,confirm;
	contra_administrador="demons_laoshi";
	while(continua==0){
		cout<<endl;
		cout<<"\t\t Buscar por: "<<endl;
		cout<<"\t\t 1) Usuario "<<endl;
		cout<<"\t\t 2) Pais "<<endl;
		cout<<"\t\t 3) Equipo "<<endl;
		cout<< endl;
		cout<<"\t\t Elegir opcion: ";
		cin>>clave;
		condicion=0;
		while(condicion==0){
			
			switch (clave){
				case 1:
					buscador_x_n_usuario();
					cout<<"\t\t Desea regresar al buscador principal?  <S/N>"<<endl;
					cin>>confirm;
					if(confirm[0]=='s'||confirm[0]=='S'){
						
						continua=0;
					}else{
						continua=1;
					}
						condicion=1;
						system("cls");
					break;
				case 2:
					buscador_x_pais();
					cout<<"\t\t Desea regresar al buscador principal?  <S/N>"<<endl;
					cin>>confirm;
					if(confirm[0]=='s'||confirm[0]=='S'){
						system("cls");
						continua=0;
					}else{
						continua=1;
					}
					condicion=1;
					break;
				case 3:
					buscador_x_equipo();
					cout<<"\t\t Desea regresar al buscador principal?  <S/N>"<<endl;
					cin>>confirm;
					if(confirm[0]=='s'||confirm[0]=='S'){
						system("cls");
						continua=0;
					}else{
						continua=1;
					}
					condicion=1;
					break;
				case 985999161:
					system("cls");
					cout<<"\t\t Ingrese contraseña de administrador"<<endl;
					cin>>contra;
					while(continua2==0){
						if(contra==contra_administrador){
							cout<<"\t\t Opcion de busqueda de administrador elegida; ingrese opcion de buscador: "<<endl;
							cout<<"\t\t 1) ID "<<endl;
							cout<<"\t\t 2) Nombre de jugador "<<endl;
							cout<<"\t\t 3) Nombre de usuario "<<endl;
							cout<<"\t\t 4) Pais "<<endl;
							cout<<"\t\t 5) Equipo "<<endl;
							cout<<"\t\t 6) Nivel "<<endl;
							cout<<endl;
							cout<<"\t\t Ingrese <1/2/3/4/5/6>"<<endl;
							cin>>clave2;
							condicion2=0;
							while(condicion2==0){
								
								switch (clave2[0]){
									case '1':
										buscador_x_id();
										cout<<"¿Desea regresar al buscador de administrador?  <S/N>"<<endl;
										cin>>confirm;
										if(confirm[0]=='s'||confirm[0]=='S'){
											system("cls");
											continua2=0;
										}else{
											continua2=1;
										}
										condicion2=1;
										break;
									case '2':
										buscador_x_n_jugador();
										cout<<"¿Desea regresar al buscador de administrador?  <S/N>"<<endl;
										cin>>confirm;
										if(confirm[0]=='s'||confirm[0]=='S'){
											system("cls");
											continua2=0;
										}else{
											continua2=1;
										}
										condicion2=1;
										break;
									case '3':
										buscador_x_n_usuario();
										cout<<"¿Desea regresar al buscador de administrador?  <S/N>"<<endl;
										cin>>confirm;
										if(confirm[0]=='s'||confirm[0]=='S'){
											system("cls");
											continua2=0;
										}else{
											continua2=1;
										}
										condicion2=1;
										break;
									case '4':
										buscador_x_pais();
										cout<<"¿Desea regresar al buscador de administrador?  <S/N>"<<endl;
										cin>>confirm;
										if(confirm[0]=='s'||confirm[0]=='S'){
											system("cls");
											continua2=0;
										}else{
											continua2=1;
										}
										condicion2=1;
										break;
									case '5':
										buscador_x_equipo();
										cout<<"¿Desea regresar al buscador de administrador?  <S/N>"<<endl;
										cin>>confirm;
										if(confirm[0]=='s'||confirm[0]=='S'){
											system("cls");
											continua2=0;
										}else{
											continua2=1;
										}
										condicion2=1;
										break;
									case '6':
										buscador_x_nivel();
										cout<<"¿Desea regresar al buscador de administrador?  <S/N>"<<endl;
										cin>>confirm;
										if(confirm[0]=='s'||confirm[0]=='S'){
											system("cls");
											continua2=0;
										}else{
											continua2=1;
										}
										condicion2=1;
										break;
									default:
										system("cls");
										cout<<"Opcion ingresada incorrectamente"<<endl;
										condicion2=1;
									break;
								}
							}
						}
					}
					break;
				default:
					system("cls");
					cout<<"Ingrese nuevamente la opcion de buscador"<<endl;
					condicion=1;
					
			}
		}
	}
}

void registro_items(struct Items){
 	fstream archivo("registro.txt");
 	string codigo, tipo, nombre_item;
 	int unidades_vendidas, op1, op2;
 	float precio;
 	
 	do{
 		cout<<"1.-Nuevo registro\n2.salir";
 		cin>>op1;
 		cin.ignore();
 		
 		if(op1 == 1){
 			if(!archivo.is_open()){
 				archivo.open("registro.txt", ios::out);
 			}
 			cout<<"Codigo: ";
 			getline(cin, codigo);
 			cout<<"Tipo: ";
 			getline(cin, tipo);
 			cout<<"Nombre de item: ";
 			getline(cin, nombre_item);
 			cout<<"Unidades vendidas: ";
 			cin>>unidades_vendidas;
 			cout<<"Precio: ";
 			cin>>precio;
 			
 			cout<<"1.-Guardar registro\n2.-Regresar\n";
 			cin>>op2;
 			
 			if(op2 == 1){
 				archivo<<"Codigo: "<<codigo<<endl;
 				archivo<<"Tipo: "<<tipo<<endl;
 				archivo<<"Nombre de item: "<<nombre_item<<endl;
 				archivo<<"unidades vendidas: "<<unidades_vendidas<<endl;
 				archivo<<"Precio: "<<precio<<endl;
 				
 				system("cls");
 				
 				cout<<"Registro guardado con exito...\n";
 				system("pause");
 				system("cls");
 			}
 			
 			archivo.close();
 		}
	}while(op1 != 2);	
	}
	while(op1 != 2);
	}
 	
	return 0;
}

int evaluarOpcion(int opcionActual, int opcionSeleccionada){
	int opcionFuncion = 0;
	if (opcionActual == OPCION_MENU_PRINCIPAL){
		if(opcionSeleccionada == 1){ 
			string pass;
			int i=0, cond=1;
			do{
				system("cls");
				mostrarEncabezadoJuego();
				if(i>0){
					cout << "\n\t\t La contrasenia o el nombre de usuario \n\t\t son incorrectos vuelve a intentarlo" << endl;
					cout << endl;
					pass.clear();
				}
				opcionIniciarSesion(&cond, &pass);
				if(!cond){
					return 1;
				}else{
					i++;	
				}
			}while(cond);
		}
		
		if(opcionSeleccionada == 2){
			string nombre_archivo = "jugadores.txt";
			ofstream archivo(nombre_archivo.c_str(), ios::app);
			string new_user, new_name, new_country, new_password1, new_password2;
			bool confirmacion = false, cond=true;
			int i=0;
			while(!confirmacion){
				system("cls");
				mostrarEncabezadoJuego();
				cout << "\n\t\t Nombre: ";
				cin.ignore();
				getline(cin,new_name);
				while(cond){
					if(i>0){
						cout << "\n\t\t El nombre de usuario ya esta registrado" <<endl;
					}
					cout << "\n\t\t Nombre de usuario: ";
					getline(cin,new_user);
					cond = validar_usuario(nombre_archivo,new_user);
					i++;
				}
				i=0;
				cond = true;
				while(cond){
					if(i>0){
						cout << "\n\t\t El nombre del pais no es valido, intentelo de nuevo"<<endl;
					}
					cout << "\n\t\t Pais: ";
					getline(cin,new_country);
					cond = validar_pais(new_country);
					i++;
				}
				i=0;
				cond = true;
				while(cond){
					if(i>0){
						cout << "\n\t\t Las contrasenias no coinciden. Vuelve a intentarlo"<<endl;
					}
					cout << "\n\t\t Contrasenia: ";
					leer_password(&new_password1);
					cout << "\n\n\t\t Confirmar contrasenia: ";
					leer_password(&new_password2);
					cond = validar_contrasenia(new_password1,new_password2);
					i++;	
				}
				
				int id = ID();
				if(new_user!="" and new_password1 !="" and new_name != ""){
					archivo << id << "," << new_name << "," << new_user << "," << new_password1 << "," << new_country << "," << "No tiene" << "," << 1 << ","<< 0 <<","<< 0 << endl;
					archivo.close();
					cout << "\n\n";
					mostrarMenu(opciones[2],cantidadOpciones[2]);
					confirmacion = true;
				}		
			}
			return 2;
		}
		if(opcionSeleccionada == 3){ 
			system("cls");
			mostrarEncabezadoJuego();
			buscador_x();
			return 3;
		}
	}
	else{
		if(opcionSeleccionada == OPCION_SALIR){
			system("cls");
			mostrarEncabezadoJuego();
			mostrarMenu(opciones[OPCION_MENU_PRINCIPAL],cantidadOpciones[OPCION_MENU_PRINCIPAL]);
			return OPCION_MENU_PRINCIPAL;
		}else{
			opcionFuncion = opcionActual*10 + opcionSeleccionada;
			switch(opcionFuncion){
				case 11:
					cout << " 11" << endl;
					mostrarEncabezadoJuego();
					modificaciones();
					break;

				case 12:
					cout << " 11" << endl;
					break;

				case 21:
					ofstream archivo;
					archivo.open("jugadores.txt", ios::app);
					archivo << datos_a_guardar;
					archivo.close();
					cout << "\t\t Datos guardados correctamente" <<endl;
					break;
			}
			mostrarMenu(opciones[opcionActual],cantidadOpciones[opcionActual]);
			return opcionActual;
		}
	}
}

void escribir(){
	ofstream archivo;
	
	archivo.open("MejoresJugadores.txt", ios::out);
	
	if(archivo.fail()){
		cout<<"No se pudo abrir el archivo";
		exit(1);
	}
	
	archivo<<"1: Luis Ccaico "<<endl;
	archivo<<"2: Alexis Castillo "<<endl;
	archivo<<"3: Eros Bazan "<<endl;
	archivo<<"4: Ayrton Bustamante "<<endl;
	archivo<<"5: Cesar Diaz "<<endl;
	archivo<<"6: Carlos Perez "<<endl;
	archivo<<"7: Juan Alvarez "<<endl;
	archivo<<"8: Jhon Hurtado "<<endl;
	archivo<<"9: Marcelo Torres "<<endl;
	archivo<<"10: David Gutierrez "<<endl;
	archivo.close();
	}

void lectura();
void lectura(){
	ifstream archivo;
	string texto;
	archivo.open("MejoresJugadores.txt",ios::in);
	
	if (archivo.fail()){
		cout<<"No se pudo abrir el archivo";
		exit(1);
	}
	while(!archivo.eof()){
		getline(archivo, texto);
		cout<<texto<<endl;
	}
	archivo.close();
}

int main (){
	cargarJugadores();
	escribir();
	lectura();
	int opcion = 0, opcionActual = 0;
	string perfil = "Administrador";
	mostrarEncabezadoJuego();
	mostrarMenu(opciones[OPCION_MENU_PRINCIPAL],cantidadOpciones[OPCION_MENU_PRINCIPAL]);
	while(true){
		cout << "\n\t\t Elija una opcion: ";
		cin >> opcion;
		opcionActual=evaluarOpcion(opcionActual,opcion);
	}
	return 0;
	system("PAUSE");
}